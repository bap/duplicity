��          L      |       �   �   �       �     �  ;   �  /   �  �  +  �         �     �  ;     /   T                                         
PYTHONOPTIMIZE in the environment causes duplicity to fail to
recognize its own backups.  Please remove PYTHONOPTIMIZE from
the environment and rerun the backup.

See https://bugs.launchpad.net/duplicity/+bug/931175
 Could not initialize backend: %s Error initializing file %s Local and Remote metadata are synchronized, no sync needed. Synchronizing remote metadata to local cache... Project-Id-Version: duplicity
Report-Msgid-Bugs-To: Kenneth Loafman <kenneth@loafman.com>
PO-Revision-Date: 2023-01-25 19:48
Last-Translator: 
Language-Team: English, Australia
Language: en_AU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: duplicity
X-Crowdin-Project-ID: 539508
X-Crowdin-Language: en-AU
X-Crowdin-File: /main/po/duplicity.pot
X-Crowdin-File-ID: 6
 
PYTHONOPTIMIZE in the environment causes duplicity to fail to
recognise its own backups.  Please remove PYTHONOPTIMIZE from
the environment and rerun the backup.

See https://bugs.launchpad.net/duplicity/+bug/931175
 Could not initialise backend: %s Error initialising file %s Local and Remote metadata are synchronised, no sync needed. Synchronising remote metadata to local cache... 